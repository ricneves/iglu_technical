﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelloApp.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string PostCode { get; set; }
        public string HouseNumber { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime? LastTimeVisited { get; set; }
        public double? DistanceKms { get; set; }
        public double? DistanceMiles { get; set; }

    }
}