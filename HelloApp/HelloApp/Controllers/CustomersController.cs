﻿using HelloApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Device.Location;
using System.Net.Http;
using System.Net;

namespace HelloApp.Controllers
{
    public class CustomersController : ApiController
    {
        List<Customer> customers = new List<Customer>
        {
            new Customer { Id = 1, PostCode = "PO63TD", HouseNumber = "1", Latitude = 50.845383, Longitude = -1.086698, LastTimeVisited=new DateTime (2019,09,01), DistanceKms=null, DistanceMiles=null },
            new Customer { Id = 2, PostCode = "SW191NE", HouseNumber = "165", Latitude = 51.419124, Longitude = -0.198316, LastTimeVisited=new DateTime (2019,08,02), DistanceKms=null, DistanceMiles=null },
            new Customer { Id = 3, PostCode = "EH11BP", HouseNumber = "Iglu Com", Latitude = 55.950917, Longitude = -3.190499, LastTimeVisited=new DateTime (2019,09,03), DistanceKms=null, DistanceMiles=null },
            new Customer { Id = 4, PostCode = "SE108XJ", HouseNumber = "Royal Observatory Greenwich", Latitude = 51.484638, Longitude = -0.001072, LastTimeVisited=new DateTime (2018,09,01) },
            new Customer { Id = 5, PostCode = "SW191NE", HouseNumber = "160", Latitude = 52.419124, Longitude = -1.198316, LastTimeVisited=new DateTime (2019,08,02), DistanceKms=null, DistanceMiles=null },
            new Customer { Id = 6, PostCode = "PO63TD", HouseNumber = "2", Latitude = 52.419124, Longitude = -1.198316, LastTimeVisited=null, DistanceKms=null, DistanceMiles=null },
        };

        /*
        public IHttpActionResult GetCustomer(int id)
        {
            
            // get customer
            var customer =  customers.Where(c=>c.Id==id).ToList();

            // get destination
            var destination = GetDestination();

            // calculate the distance
            CalculateDistance(customer, destination);

            if (customer == null)
            {
                return NotFound();
            }
            return Ok(customer);
        }
        */

        //public HttpResponseMessage Get([FromUri] CustomerSearchOptions searchOptions)
        public HttpResponseMessage Get(string postcode, string housenumber="")
        {
            if (postcode == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            try
            {
                // filter by postcode, housenumber
                var costFiltered = customers.Where(c=> c.PostCode == postcode && (
                        !String.IsNullOrEmpty(housenumber)? c.HouseNumber == housenumber : true)
                        ).ToList();

                // get destination
                var destination = GetDestination();

                // for each element calculate the distance
                CalculateDistance(costFiltered, destination);

                return Request.CreateResponse(HttpStatusCode.OK, costFiltered);
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        //public List<Customer> GetAllCustomers()
        //{
        //    return customers.OrderByDescending(c => c.LastTimeVisited).ToList();
        //}

        [HttpGet]
        [Route("api/customers/lastvisited")]
        public List<Customer> GetCustomerByLastVisited()
        {
            var list = (from t in customers
                        where t.LastTimeVisited != null
                        orderby t.LastTimeVisited descending
                        select t).Take(3).ToList();

            return list;
        }

        #region Private Methods

        private Destination GetDestination(int id=0)
        {
            Destination destination = new Destination { Id = 1, Name = "London Heathrow airport", Latitude = 51.4700223, Longitude = -0.4542955 };
            return destination;
        }

        
        private Tuple<double,double> GetDistance(double sLatitude, double sLongitude, double dLatitude, double dLongitude)
        {
            
            var sCoord = new GeoCoordinate(sLatitude, sLongitude);
            var dCoord = new GeoCoordinate(dLatitude, dLongitude);

            // return Kms, miles
            return Tuple.Create(sCoord.GetDistanceTo(dCoord)/1000, (sCoord.GetDistanceTo(dCoord)/1000)*0.621371192);
            
        }

        private List<Customer> CalculateDistance(List<Customer> customer, Destination dest)
        {

            foreach (Customer c in customer)
            {
                var distance = GetDistance(c.Latitude, c.Longitude, dest.Latitude, dest.Longitude);
                c.DistanceKms = distance.Item1;
                c.DistanceMiles = distance.Item2;

            }

            return customer;
        }

        #endregion
    }
}
